//********************************************************************
//  BMIGUI.java     
//
//  Wyznacza BMI w GUI.
//********************************************************************

import java.awt.*;
import java.awt.event.*;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

import javax.swing.*;

public class BMIGUI
{
   private int WIDTH = 260;
   private int HEIGHT = 160;

   private JFrame frame;
   private JPanel panel;
   private JLabel wzrostLabel, wagaLabel, BMILabel, wynikLabel;
   private JTextField wzrost, waga;
   private JButton oblicz;

   //-----------------------------------------------------------------
   //  Ustawia GUI.
   //-----------------------------------------------------------------
   public BMIGUI()
   {
      frame = new JFrame ("Kalkulator BMI");
      frame.setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);

      //tworzenie etykiety dla pol tekstowych wzrostu i wagi 
      wzrostLabel = new JLabel ("Twoj wzrost w metrach:");
      wagaLabel = new JLabel ("Twoja waga w kilokramach: ");

      BMILabel = new JLabel ("Twoje BMI: ");
      wynikLabel = new JLabel ();

      wzrost = new JTextField(20);
      waga = new JTextField(20);

      oblicz = new JButton("Oblicz");
      oblicz.setPreferredSize(new Dimension(200,30));
      
      // stworz BMIListener, ktory bedzie nasluchiwal czy przycis zostal nacisniety 
      oblicz.addActionListener(new BMIListener());

      // ustawienia JPanel znajdujacego sie na JFrame 
      panel = new JPanel();
      panel.setPreferredSize (new Dimension(WIDTH, HEIGHT));
      panel.setBackground (Color.white);

      panel.setLayout (new FlowLayout());
      panel.add(wzrostLabel);
      panel.add(wzrost);
      panel.add(wagaLabel);
      panel.add(waga);
      panel.add(BMILabel);
      panel.add(wynikLabel);
      panel.add(oblicz);

      //dodaj panel do frame 
      frame.setResizable(false);
      frame.getContentPane().add (panel);
   }

   //-----------------------------------------------------------------
   //  Wyswietl frame aplikacji podstawowej
   //-----------------------------------------------------------------
   public void display()
   {
      frame.pack();
      frame.show();
   }

   //*****************************************************************
   //  Reprezentuje action listenera dla przycisku oblicz.
   //*****************************************************************
   private class BMIListener implements ActionListener
   {
      //--------------------------------------------------------------
      //  Wyznacz BMI po wcisnieciu przycisku
      //--------------------------------------------------------------
      public void actionPerformed (ActionEvent event)
      {
         double bmi;

         double wzrostDouble = Double.parseDouble(wzrost.getText());
         double wagaDouble = Double.parseDouble(waga.getText());
         
         bmi = wagaDouble/(wzrostDouble*wzrostDouble);
        
         final String[] komunikaty = {"niedowaga","waga prawid�owa","nadwaga","oty�o�c"};
         String komunikat = new String();
        
         if (bmi<19)
        	 komunikat = komunikaty[0];
         else
        	 if (bmi<25)
        		 komunikat = komunikaty[1];
        	 else
        		 if (bmi<30)
        			 komunikat = komunikaty[2];
        		 else
        			 komunikat = komunikaty[3];
         
         DecimalFormat formatter = new DecimalFormat("#.##", DecimalFormatSymbols.getInstance( Locale.ENGLISH ));
         formatter.setRoundingMode( RoundingMode.DOWN );
         
         wynikLabel.setText("" + formatter.format(bmi) + " - " + komunikat);

      }
   }
}

