// ***********************************************************************
//  LicznikGlosow.java
//
//  Wykorzystuje GUI oraz listenery eventow do glosowania 
//  na dwoch kandydatow -- Jacka i Placka.
//
// ***********************************************************************

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class LicznikGlosow extends JApplet
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int APPLET_WIDTH = 400, APPLET_HEIGHT=100;
    private int glosyDlaJacek;
    private JLabel labelJacek;
    private JButton Jacek;
    private int glosyDlaPlacek;
    private JLabel labelPlacek;
    private JButton Placek;
    
    // ------------------------------------------------------------
    //  Ustawia GUI
    // ------------------------------------------------------------
    public void init ()
    {
	glosyDlaJacek = 0;
	glosyDlaJacek = 0;
	
	Jacek = new JButton ("Glosuj na Komorowskiego!");
	Jacek.addActionListener (new JacekButtonListener());
	Placek = new JButton ("Glosuj na Dud�!");
	Placek.addActionListener (new PlacekButtonListener());
	
	labelJacek = new JLabel ("Glosy dla Komora: " + Integer.toString (glosyDlaJacek));
	labelPlacek = new JLabel ("Glosy dla Dudy: " + Integer.toString (glosyDlaPlacek));
	
	Container cp = getContentPane();
	cp.setBackground (Color.cyan);
	cp.setLayout (new FlowLayout());
	cp.add (Jacek);
	cp.add (labelJacek);
	cp.add (Placek);
	cp.add (labelPlacek);

	setSize (APPLET_WIDTH, APPLET_HEIGHT);
    }


    // *******************************************************************
    //  Reprezentuje listener dla akcji wcisniecia przycisku
    // *******************************************************************
    private class JacekButtonListener implements ActionListener
    {
        public void actionPerformed (ActionEvent event)
        {
            glosyDlaJacek++;
            labelJacek.setText ("Glosy dla Jacka: " + Integer.toString (glosyDlaJacek)); 
            repaint ();
        }
    }
    private class PlacekButtonListener implements ActionListener
    {
        public void actionPerformed (ActionEvent event)
        {
            glosyDlaPlacek++;
            labelPlacek.setText ("Glosy dla Placka: " + Integer.toString (glosyDlaPlacek)); 
            repaint ();
        }
    }
}

